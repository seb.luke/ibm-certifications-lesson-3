package com.certification.lesson3.controller;

import com.certification.lesson3.exceptions.LessonException;
import com.certification.lesson3.services.PersonService;
import com.certification.lesson3.dto.PersonDto;
import com.certification.lesson3.entity.Person;
import com.certification.lesson3.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @GetMapping("/getAllPersons")
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @GetMapping("/get/{personId}")
    public PersonDto getPerson(@PathVariable Long personId) {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new LessonException(HttpStatus.NOT_FOUND, String.format("Person with id '%s' does not exist.", personId)));
        return personService.toDto(person);
    }

    @GetMapping("/getAllPersonsDto")
    public List<PersonDto> getAllPersonsDto() {
        return personService.toDtoList(personRepository.findAll());
    }

    @PostMapping("/updatePerson")
    public void updatePerson(@RequestBody PersonDto personDto) {
        personRepository.save(personService.toEntity(personDto));
    }

    @PutMapping("/addPerson")
    public PersonDto addPerson(@RequestBody PersonDto personDto) {
        failIfDtoContainsIds(personDto);
        Person person = personService.toEntity(personDto);
        Person newPerson = personRepository.save(person);
        return personService.toDto(newPerson);
    }

    private void failIfDtoContainsIds(PersonDto personDto) {
        if (personDto.getId() != null) {
            throw new LessonException(HttpStatus.EXPECTATION_FAILED, "New person should not have an ID");
        }

        personDto.getAddressList().stream()
                .filter(a -> a.getId() != null)
                .findFirst()
                .ifPresent(a -> {throw new LessonException(HttpStatus.EXPECTATION_FAILED, "New address should not contain an ID");});
    }

}
