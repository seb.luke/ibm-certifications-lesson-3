# IBM Certifications - Lesson 3
### - DTOs and HTTP status codes - 

### Recommended links
For further understanding, please look into the following articles

* [Lesson 3 Theory @ Prezi](https://prezi.com/view/579R3zWbNUy4GXDDBqN5/)
* [Why use DTOs? @ CodeOpinion](https://codeopinion.com/why-use-dtos-data-transfer-objects/)
* [Entity to DTO @ Baeldung](https://www.baeldung.com/entity-to-and-from-dto-for-a-java-spring-application)
* [HTTP Status Codes @ Mozilla Developer](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
* [List of all HTTP Status Codes @ Wikipedia](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

<hr>
For more questions, please be free to contact me via the WhatsApp groups 