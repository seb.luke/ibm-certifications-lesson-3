package com.certification.lesson3.services;

import com.certification.lesson3.dto.AddressDto;
import com.certification.lesson3.entity.Address;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressService {

    @Autowired
    private ModelMapper modelMapper;

    public AddressDto toDto(Address address) {
        return modelMapper.map(address, AddressDto.class);
    }

    public List<AddressDto> toDtoList(List<Address> addressList) {
        return addressList.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public Address toEntity(AddressDto addressDto) {
        return modelMapper.map(addressDto, Address.class);
    }

    public List<Address> toEntityList(List<AddressDto> addressDtoList) {
        return addressDtoList.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
