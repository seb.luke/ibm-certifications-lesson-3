package com.certification.lesson3.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
// used instead of @Data because automatic hash code implementation calls hashcode of addressList entries which generate
// a circular dependency and imminently a StackOverflowException
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode(exclude = "addressList")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private ZonedDateTime lastLogin;

    @Column(nullable = false)
    private String socialSecurity;

    @Column(nullable = false)
    private boolean isMarried = false;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    private final Set<Address> addressList = new HashSet<>();

    public void addAddress(Address newAddress) {
        this.addressList.add(newAddress);
        newAddress.setPerson(this);
    }

    public void removeAddress(Address address) {
        this.addressList.remove(address);
        address.setPerson(null);
    }
}
