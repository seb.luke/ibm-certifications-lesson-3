package com.certification.lesson3.config;

import com.certification.lesson3.entity.Address;
import com.certification.lesson3.entity.Person;
import com.certification.lesson3.repository.PersonRepository;
import com.certification.lesson3.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Configuration
public class BasicConfiguration {

    @Bean
    public CommandLineRunner insertDemoData(PersonRepository personRepository) {
        return args -> {
            Person p1 = new Person();
            p1.setName("Person 1");
            p1.setLastLogin(ZonedDateTime.now(ZoneId.systemDefault()));
            p1.setSocialSecurity("123456");

            p1.addAddress(new Address("Country", "City 1", "Street 1", 251));
            p1.addAddress(new Address("Country", "City 2", "Street 2", 12));

            Person p2 = new Person();
            p2.setName("Person 2");
            p2.setLastLogin(ZonedDateTime.of(LocalDateTime.of(2019, 11, 24, 23, 55), ZoneId.systemDefault()));
            p2.setSocialSecurity("987665");
            p2.setMarried(true);

            p2.addAddress(new Address("Country 2", "City 3", "Street 3", 999));
            p2.addAddress(new Address("Country 2", "City 4", "Street 4", 981));

            personRepository.save(p1);
            personRepository.save(p2);
        };
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public CommandLineRunner instantiateUsername(UserService userService) {
        return args -> userService.setUsername("NotAnAdmin");
    }
}
