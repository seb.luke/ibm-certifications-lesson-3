package com.certification.lesson3.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.*;

@Data
@JsonInclude(NON_NULL)
public class PersonDto {

    private Long id;
    private String name;
    private String lastLogin;
    private List<AddressDto> addressList;
    private String ssn; // SocialSecurityNumber but I don't want it automatically mapped therefore I use a different name
    private String loggedInUsername;

    public ZonedDateTime getLastLoginConverted(String zoneId) {
        return ZonedDateTime.of(LocalDateTime.parse(this.lastLogin, DateTimeFormatter.ISO_DATE_TIME), ZoneId.of(zoneId));
    }

    public void setLastLogin(ZonedDateTime dateTime) {
        this.lastLogin = dateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }
}
