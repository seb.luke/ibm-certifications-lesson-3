package com.certification.lesson3.services;

import com.certification.lesson3.dto.PersonDto;
import com.certification.lesson3.entity.Person;
import com.certification.lesson3.repository.PersonRepository;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserService userService;

    public PersonDto toDto(Person person) {
        PersonDto dto = modelMapper.map(person, PersonDto.class);
        dto.setLastLogin(person.getLastLogin());
        dto.setLoggedInUsername(userService.getUsername());

        return dto;
    }

    public List<PersonDto> toDtoList(List<Person> personList) {
        return personList.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public Person toEntity(PersonDto personDto) {
        Person person = modelMapper.map(personDto, Person.class);
        person.setLastLogin(personDto.getLastLoginConverted(ZoneId.systemDefault().toString()));

        if (person.getId() != null) {
            Person dbPerson = personRepository.getOne(person.getId());
            person.setSocialSecurity(dbPerson.getSocialSecurity());
            person.setMarried(dbPerson.isMarried());
        }

        if (StringUtils.isNotEmpty(personDto.getSsn())) {
            person.setSocialSecurity(personDto.getSsn());
        }

        personDto.getAddressList().stream()
                .map(addressService::toEntity)
                .forEach(person::addAddress);

        return person;
    }
}
